class City(models.Model):
    state = models.ForeignKey(State, related_name='cities')
    name = models.TextField()
    population = models.DecimalField()
    land_area_km = models.DecimalField()
    def density(self):
       return self.population / self.land_area_km


class CitySet(models.QuerySet):
    def add_density(self):
        return self.annotate(
            density=F('population') / F('land_area_km')
        )
    def dense_cities(self):
        self.add_density().filter(density__gt=4000)